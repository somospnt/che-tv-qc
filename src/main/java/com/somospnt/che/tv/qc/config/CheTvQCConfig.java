package com.somospnt.che.tv.qc.config;

import com.somospnt.che.tv.qc.domain.Bug;
import com.somospnt.che.tv.qc.transformer.BugToAnuncioTransformer;
import com.somospnt.che.tv.qc.transformer.MailTransformer;
import java.util.Properties;
import org.springframework.http.HttpMethod;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.http.Http;
import org.springframework.integration.endpoint.MethodInvokingMessageSource;
import org.springframework.integration.json.ObjectToJsonTransformer;
import org.springframework.integration.mail.Pop3MailReceiver;
import org.springframework.messaging.Message;

@Configuration
public class CheTvQCConfig {

    @Value("${che.tv.qc.mail.host}")
    private String host;
    @Value("${che.tv.qc.mail.port}")
    private int port;
    @Value("${che.tv.qc.mail.username}")
    private String username;
    @Value("${che.tv.qc.mail.password}")
    private String password;
    @Value(value = "${che.tv.qc.api}")
    private String urlTvSomospntUpdate;

    @Value(value = "${che.tv.qc.api.delete}")
    private String urlTvSomospntDelete;
    
    @Value(value = "${che.tv.qc.periodo.milisegundos}")
    private long periodoMilisegundos;

    @Bean
    public MessageSource<?> integerMessageSource() {
        Pop3MailReceiver mailReceiver = new Pop3MailReceiver(host, port, username, password);

        Properties properties = new Properties();
        properties.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.pop3.socketFactory.port", "995");
        properties.setProperty("mail.pop3.socketFactory.fallback", "false");
        properties.setProperty("mail.debug", "false");
        properties.setProperty("mail.pop3.port", "995");

        mailReceiver.setJavaMailProperties(properties);
        MethodInvokingMessageSource source = new MethodInvokingMessageSource();
        source.setObject(mailReceiver);
        source.setMethodName("receive");
        return source;
    }

    @Bean
    public IntegrationFlow cheTvIntegrationFlow() {
        return IntegrationFlows.from(integerMessageSource(),
                c -> c.poller(p -> p.fixedRate(periodoMilisegundos))
                .autoStartup(true))
                .handle(mailTransformer())
                .split()
                .routeToRecipients(r -> r
                        .recipient("actualizarBug",
                                m -> bugDeNuestroLado(m))
                        .recipient("eliminarBug",
                                m -> !bugDeNuestroLado(m)))
                .get();
    }

    @Bean
    public IntegrationFlow actualizarBugFlow() {
        return IntegrationFlows.from("actualizarBug")
                .handle(bugToAnuncioTransformer())
                .transform(new ObjectToJsonTransformer())
                .handle(Http.outboundGateway(urlTvSomospntUpdate)
                        .httpMethod(HttpMethod.PUT)
                        .extractPayload(true))
                .handle(System.out::println)
                .get();
    }

    @Bean
    public IntegrationFlow eliminarBugFlow() {
        return IntegrationFlows.from("eliminarBug")
                .transform("payload.codigo")
                .handle(Http.outboundGateway(urlTvSomospntDelete)
                        .httpMethod(HttpMethod.DELETE)
                        .uriVariable("clave", "payload"))
                .handle(System.out::println)
                .get();
    }

    @Bean
    public MailTransformer mailTransformer() {
        return new MailTransformer();
    }

    @Bean
    public BugToAnuncioTransformer bugToAnuncioTransformer() {
        return new BugToAnuncioTransformer();
    }

    private boolean bugDeNuestroLado(Message<?> msg) {
        Bug bug = (Bug) msg.getPayload();
        return bug.getStatus().equals("New") || bug.getStatus().equals("Open");
    }
}
