package com.somospnt.che.tv.qc.domain;

import java.io.Serializable;

public class Anuncio implements Serializable {

    private String clave;
    private String titulo;
    private String contenido1;
    private String contenido2;
    private String contenido3;
    private String tipo = "simple";
    private String nivel = "ERROR";

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido1() {
        return contenido1;
    }

    public void setContenido1(String contenido1) {
        this.contenido1 = contenido1;
    }

    public String getContenido2() {
        return contenido2;
    }

    public void setContenido2(String contenido2) {
        this.contenido2 = contenido2;
    }

    public String getContenido3() {
        return contenido3;
    }

    public void setContenido3(String contenido3) {
        this.contenido3 = contenido3;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    @Override
    public String toString() {
        return "Anuncio{" + "clave=" + clave + ", titulo=" + titulo + ", contenido1=" + contenido1 + ", contenido2=" + contenido2 + ", contenido3=" + contenido3 + ", tipo=" + tipo + ", nivel=" + nivel + '}';
    }

}
