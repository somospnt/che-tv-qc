package com.somospnt.che.tv.qc.transformer;

import com.somospnt.che.tv.qc.domain.Anuncio;
import com.somospnt.che.tv.qc.domain.Bug;
import org.springframework.integration.transformer.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

public class BugToAnuncioTransformer implements Transformer {

    @Override
    public Message<?> transform(Message<?> msg) {
        Bug bug = (Bug) msg.getPayload();
        Anuncio anuncio = new Anuncio();
        anuncio.setClave(bug.getCodigo());
        anuncio.setTitulo("QC " + bug.getCodigo());
        anuncio.setContenido1("Estado: " + bug.getStatus());
        anuncio.setContenido2(bug.getNombreProyecto());
        anuncio.setContenido3(bug.getFecha());
        return new GenericMessage(anuncio);
    }

}
