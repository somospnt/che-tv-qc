package com.somospnt.che.tv.qc.transformer;

import com.somospnt.che.tv.qc.domain.Bug;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.transformer.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

public class MailTransformer implements Transformer {

    private static final Logger logger = LoggerFactory.getLogger(MailTransformer.class);

    @Override
    public Message<?> transform(Message<?> msg) {

        MimeMessage[] mimeMessages = (MimeMessage[]) msg.getPayload();
        logger.debug("{} Emails recibidos. Comenzando procesamiento.", mimeMessages.length);
        if (mimeMessages.length > 0) {
            ArrayList<Bug> bugs = new ArrayList<>();
            try {
                for (MimeMessage mensaje : ((MimeMessage[]) msg.getPayload())) {
                    logger.debug("Email recibido. Subject: {}", mensaje.getSubject());
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    mensaje.writeTo(out);
                    String mail = out.toString();
                    Bug bug = crearBugFromString(mail);
                    if (bug != null) {
                        bugs.add(bug);
                    }
                }
                return new GenericMessage(bugs);
            } catch (IOException | MessagingException ex) {
                logger.error("Error procesando mails", ex);
            }
        }
        return null;

    }

    private Bug crearBugFromString(String mail) {
        String estado = StringUtils.substringBetween(mail, "Status*", "*Asignado");
        String codigo = StringUtils.substringBetween(mail, "*ID Incidente* ", "*Status");
        String nombreProyecto = StringUtils.substringBetween(mail, "*Aplicacion* ", "*Clasifi");
        String fecha = StringUtils.substringBetween(mail, "*Fecha de Deteccion* ", "*Tipo");
        if (StringUtils.isEmpty(estado) || StringUtils.isEmpty(codigo) || StringUtils.isEmpty(nombreProyecto) || StringUtils.isEmpty(fecha)) {
            return null;
        }
        Bug bug = new Bug();
        bug.setStatus(estado.trim());
        bug.setCodigo(codigo.trim());
        bug.setNombreProyecto(nombreProyecto.trim());
        bug.setFecha(fecha.trim());
        return bug;
    }
}
