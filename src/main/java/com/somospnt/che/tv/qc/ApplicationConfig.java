package com.somospnt.che.tv.qc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationConfig {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(ApplicationConfig.class, args);
    }

}
