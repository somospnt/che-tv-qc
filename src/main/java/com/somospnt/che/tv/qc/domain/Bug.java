package com.somospnt.che.tv.qc.domain;

import java.io.Serializable;

public class Bug implements Serializable {

    private String codigo;
    private String status;
    private String nombreProyecto;
    private String fecha;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "Bug{" + "codigo=" + codigo + ", status=" + status + ", nombreProyecto=" + nombreProyecto + ", fecha=" + fecha + '}';
    }

}
