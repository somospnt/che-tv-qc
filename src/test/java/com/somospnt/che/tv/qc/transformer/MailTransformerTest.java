package com.somospnt.che.tv.qc.transformer;

import com.somospnt.che.tv.qc.domain.Bug;
import java.io.IOException;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import org.apache.commons.io.IOUtils;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

public class MailTransformerTest {

    @Test
    public void tranform_conMailValido_devuelveQCObjeto() throws IOException, MessagingException {
        String mail = IOUtils.toString(this.getClass().getResourceAsStream("/mails/QC-Open.txt"), "UTF-8");
        MailTransformer mailTransformer = new MailTransformer();
        MimeMessage[] mimeMessageArray = new MimeMessage[1];
        mimeMessageArray[0] = new MimeMessage((Session) null);
        mimeMessageArray[0].setContent(mail, "text/html");
        Message genericMessage = new GenericMessage(mimeMessageArray);

        Message messageBug = mailTransformer.transform(genericMessage);
        List<Bug> bugs = (List<Bug>) messageBug.getPayload();
        assertNotNull(bugs);
        Bug bug = bugs.get(0);
        assertEquals("42670", bug.getCodigo());
        assertEquals("PORTAL DE GESTION", bug.getNombreProyecto());
        assertEquals("New", bug.getStatus());
        assertEquals("2016-09-14", bug.getFecha());
    }

    @Test
    public void transform_conMailCualquiera_devuelveListaVacia() throws IOException, MessagingException {
        String mail = IOUtils.toString(this.getClass().getResourceAsStream("/mails/mailCualquiera.txt"), "UTF-8");
        MailTransformer mailTransformer = new MailTransformer();
        MimeMessage[] mimeMessageArray = new MimeMessage[1];
        mimeMessageArray[0] = new MimeMessage((Session) null);
        mimeMessageArray[0].setContent(mail, "text/html");
        Message genericMessage = new GenericMessage(mimeMessageArray);

        Message messageBug = mailTransformer.transform(genericMessage);
        List<Bug> bugs = (List<Bug>) messageBug.getPayload();
        assertNotNull(bugs);
        assertTrue(bugs.isEmpty());
    }
}
